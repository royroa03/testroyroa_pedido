<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CombosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('combos')->insert([
            'nombre' => 'Combo Doble',
            'fecha_especial' => Carbon::create('2020', '01', '01')
        ]);
        DB::table('combos')->insert([
            'nombre' => 'Combo Trío',
            'fecha_especial' => Carbon::create('2020', '04', '30')
        ]);
        DB::table('combos')->insert([
            'nombre' => 'Combo Nocturno',
            'fecha_especial' => Carbon::create('2020', '03', '31')
        ]);
        DB::table('combos')->insert([
            'nombre' => 'Combo Parrillero',
            'fecha_especial' => Carbon::create('2020', '04', '01')
        ]);
    }
}
