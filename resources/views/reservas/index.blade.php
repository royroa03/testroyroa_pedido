<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">


        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref p-5">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Reserva de Pedido
                </div>

                <div class="container"> 
                    <div class="row">
                        <div class="col-sm-12">
                            @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <label>{{ $error }}</label>
                                    @endforeach
                                </ul>
                            </div><br />
                            @endif
                            {!! Form::open(['url'=> 'reservas']) !!}
                                {{ Form::token() }}
                                <div class="form-group">
                                    <label for="nombre">(*) Nombres</label>
                                    <input type="text" class="form-control" name="nombre" placeholder="Escribe tus nombres" required>
                                </div>
                                <div class="form-group">
                                    <label for="apellido">(*) Apellidos</label>
                                    <input type="text" class="form-control" name="apellido" placeholder="Escribe tus apellidos" required>
                                </div>
                                <div class="form-group">
                                    <label for="email">(*) Email</label>
                                    <input type="email" class="form-control" name="email" placeholder="Escribe tu email" required>
                                </div>
                                <div class="form-group">
                                    <label for="distrito">(*) Distrito</label>
                                    <input type="text" class="form-control" name="distrito" placeholder="Escribe tu distrito" required>
                                </div>
                                <div class="form-group">
                                    <label for="telefono">(*) Teléfono</label>
                                    <input type="text" class="form-control" name="telefono" placeholder="Escribe tu teléfono" required>
                                </div>
                                <div class="form-group">
                                    <label for="fecha_registro">(*) Fecha y Hora</label>
                                    <input type="datetime-local" class="form-control" name="fecha_registro" required>
                                </div>
                                <div class="form-group">
                                    <label for="cantidad_persona">(*) Cantidad de Personas</label>
                                    <input type="number" class="form-control" name="cantidad_persona" required>
                                </div>
                                <div class="form-group">
                                    <label for="email">(*) Combo</label>
                                    <select name="id_combo" class="form-control">
                                        <option value="">--- Seleccionar Combo ---</option>
                                        @foreach ($combos as $key => $value)
                                        <option value="{{ $value->id }}">({{ $value->fecha_especial }}) {{ $value->nombre }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="comentario">Comentario</label>
                                    <textarea class="form-control" name="comentario" ></textarea>
                                </div>
                                
                                <button type="submit" class="btn btn-primary btn-block">Registrar Pedido</button>
                            {!! Form::close() !!}
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </body>
</html>
