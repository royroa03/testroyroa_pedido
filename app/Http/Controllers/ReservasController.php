<?php

namespace App\Http\Controllers;

use App\Reservas;
use App\Combos;
use Illuminate\Http\Request;
use App\Http\Requests\ReservasFormRequest;

use App\Http\Resources\ReporteReservasCollection;

class ReservasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $combos = Combos::all();
        return view('reservas.index', ['combos'=>$combos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ReservasFormRequest $request)
    {
        $reserva = new Reservas();

        $reserva->nombre = request('nombre');
        $reserva->apellido = request('apellido');
        $reserva->email = request('email');
        $reserva->distrito = request('distrito');
        $reserva->telefono = request('telefono');
        $reserva->fecha_registro = request('fecha_registro');
        $reserva->cantidad_persona = request('cantidad_persona');
        $reserva->id_combo = request('id_combo');
        $reserva->comentario = request('comentario');

        $reserva->save();
        
        return view('reservas.mensaje',['reserva'=>$reserva ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Reservas  $reservas
     * @return \Illuminate\Http\Response
     */
    public function show(Reservas $reservas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Reservas  $reservas
     * @return \Illuminate\Http\Response
     */
    public function edit(Reservas $reservas)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Reservas  $reservas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Reservas $reservas)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Reservas  $reservas
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reservas $reservas)
    {
        //
    }

    public function reporte(Request $request)
    {
        $reservas = Reservas::select('reservas.*', 'combos.nombre as combo')
        ->join('combos', 'combos.id', '=', 'reservas.id_combo')
        ->orderBy('reservas.id','asc')
        ->get();
        return new ReporteReservasCollection($reservas);    
        
    }

    public function reportesearch($search)
    {
        
        if($search != '' || $search != null)
        {
            $reservas = Reservas::select('reservas.*', 'combos.nombre as combo')
                    ->join('combos', 'combos.id', '=', 'reservas.id_combo')
                    ->where('reservas.nombre','LIKE','%'.$search.'%')
                    ->orderBy('reservas.id','asc')
                    //->simplePaginate(5);
                    ->paginate(10);
                    //->get();

            return new ReporteReservasCollection($reservas);    
        }
        //$reservas = Reservas::all();
        //return view('reservas.index', ['reservas'=>$reservas]);
           
        
    }

    

}
