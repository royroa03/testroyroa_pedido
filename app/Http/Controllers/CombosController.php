<?php

namespace App\Http\Controllers;

use App\Combos;
use Illuminate\Http\Request;

class CombosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Combos  $combos
     * @return \Illuminate\Http\Response
     */
    public function show(Combos $combos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Combos  $combos
     * @return \Illuminate\Http\Response
     */
    public function edit(Combos $combos)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Combos  $combos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Combos $combos)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Combos  $combos
     * @return \Illuminate\Http\Response
     */
    public function destroy(Combos $combos)
    {
        //
    }
}
