<?php

use Illuminate\Http\Request;
use App\Reservas;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
/*
Route::get('/reportereservas', function () {
    return new ReporteReservasCollection(
        Reservas::select('reservas.*', 'combos.nombre as combo')
        ->join('combos', 'combos.id', '=', 'reservas.id_combo')
        ->orderBy('reservas.id','asc')
        //->simplePaginate(5);
        ->get()
    );
});
*/
Route::get('/reportereservas', 'ReservasController@reporte');
Route::get('/reportereservas/{search}', 'ReservasController@reportesearch');